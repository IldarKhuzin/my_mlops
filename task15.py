# 1 задача. Создать ETL-скрипт. Нагенерировать данные (либо можно приложить к тесту csv)
# и сделать подневную агрегацию для каждого пользователя через Pandas или Spark*

# импортируем библиотеки

import pandas as pd
import numpy as np
import random
import datetime
from random import randrange
from datetime import timedelta, datetime
import time

# функция генерации случайной даты

def random_date(start, end):
    """
    эта функция возвращает случайную дату между двумя заданными
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

d1 = datetime.strptime('1/1/2015 1:30 PM', '%m/%d/%Y %I:%M %p')
d2 = datetime.strptime('1/1/2022 4:50 AM', '%m/%d/%Y %I:%M %p')
d3 = datetime.strptime('1/1/2021 4:50 AM', '%m/%d/%Y %I:%M %p')

# генерим данные пользователей 1000 строк

# сначала создаем пустые списки

balans = []
data_dobavleniya = []
age = []
town = []
time_of_last_activity = []
tariff = []
town_list = ['Москва','Новосибирск','Казань','Пермь','Краснодар','Чебоксары','Урюпинск','Санкт-Петербург','Мытищи','Королев']

# заполняем списки в цикле

for i in range(1000):
    balans.append(random.randint(0,1000))
    data_dobavleniya.append(random_date(d1,d2))
    age.append(random.randint(10,80))
    town.append(random.choice(town_list))
    tariff.append(random.randint(1,5))
    time_of_last_activity.append(random_date(data_dobavleniya[i],d2))

# создаем словарь данных

Data_users = {
    'текущий баланс': balans,
    'дата добавления': data_dobavleniya,
    'возраст': age,
    'город проживания': town,
    'временная метка последней активности': time_of_last_activity,
    'активный тариф': tariff
}

# создаем датафрейм

Users = pd.DataFrame(Data_users)

# создаем тарифы

Data_tariffs = {
    'название': ['Без переплат','Максимум','С переплатами','Экстремум','Минимум'],
    'дата начала': ['2020-01-01','2019-01-01','2018-01-01','2017-01-01','2016-01-01'],
    'дата конца': ['2022-01-01','2022-01-01','2022-01-01','2022-01-01','2022-01-01'],
    'объем минут': [100,500,300,200,50],
    'объем СМС': [300,200,100,400,30],
    'объем трафика': [500,1000,500,400,200]
}

Tariffs = pd.DataFrame(Data_tariffs)

# генерим события (миллион событий)

# создаем пустые списки

data_event = []
id_user = []
scope_of_servis = []
servis = []
servis_type = ['звонок', 'SMS', 'траффик']

# заполняем их в цикле

for i in range(1000000):
    data_event.append(random_date(d3,d2))
    id_user.append(random.randint(0,1000))
    servis.append(random.choice(servis_type))
    scope_of_servis.append(random.randint(1,20))

Data_events = {
    'метка времени': data_event,
    'id абонента' : id_user,
    'тип услуги' : servis,
    'объем услуги' : scope_of_servis
}

# создаем датафрейм

Events = pd.DataFrame(Data_events)


user_need = int(input('введите id пользователя(0-1000) '))                     # запрашиваем id нужного абонента
df_user_need = Events[Events['id абонента'] == user_need]                      # создаем промежуточный датафрейм с id абонента
df_user_need['день'] = df_user_need['метка времени'].dt.strftime('%Y-%m-%d')   # меняем формат времени в этом датафрейме
df_user_need.drop('метка времени', axis=1, inplace=True)                       # удаляем столбец "метка времени"

# группируем по номеру абонента, дню пользования услугой и по типу услуги

df_finish = df_user_need.groupby(['id абонента','день','тип услуги']).agg(sum).unstack(fill_value=0)
print('')
print('')
print(df_finish)                                                               # выводим результат
